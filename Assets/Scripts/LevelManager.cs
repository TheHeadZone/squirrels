﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevelManager : MonoBehaviour
{
    private static LevelManager _instance;

    [SerializeField] Door door;

    [SerializeField] Coin[] coins;

    [SerializeField] Jukebox jukebox;

    int currentCoinIndex = 0;


    void Awake(){
        if(_instance == null)
            _instance = this;

        else if(_instance != this)
            Destroy(this.gameObject);
    }
    


    public static LevelManager Instance
    {
        get
        {
            return _instance;
        }
    }

    /* void Update(){

        if(Input.GetKeyDown(KeyCode.E)){
        }
    }*/

    public Door Door{
        get{
            return this.door;
        }
    }

    public Coin ActiveCoin{
        get{
            return coins[currentCoinIndex];
        }
    }

    public void ConsumeCoin(){
        ++currentCoinIndex;
    }

    public Jukebox Jukebox{
        get{
            return jukebox;
        }
    }


}
