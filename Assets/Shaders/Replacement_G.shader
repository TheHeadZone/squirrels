﻿// Upgrade NOTE: replaced 'mul(UNITY_MATRIX_MVP,*)' with 'UnityObjectToClipPos(*)'


Shader "Unlit/Replacement"
{
    Properties
    {
        _Color("Color", Color) = (1, 1, 1, 1)
    }
    
     SubShader
    {
        Tags { "RenderType"="Opaque" }
        LOD 100

        Pass
        {
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag

            #include "UnityCG.cginc"

            half4 _Color;


            struct appdata
            {
                float4 vertex : POSITION;
            };

            struct v2f
            {
                float4 vertex : SV_POSITION;
                float depth : DEPTH;
            };

            v2f vert (appdata v)
            {
                v2f o;
                o.vertex = UnityObjectToClipPos(v.vertex);
                //Multiplies vertex times model and view matrix, leaving it on 
                //camera coordinates, then, multiplying it by projectionParams
                //which is equal to 1 / farplane, which leaves us with a 
                //normalized value
                o.depth = -mul(UNITY_MATRIX_MV, v.vertex).z * _ProjectionParams.w;

                return o;
            }

            fixed4 frag (v2f i) : SV_Target
            {
                float invert = 1 - i.depth;
                return fixed4(invert, invert, invert, 1) * _Color;
            }
            ENDCG
        }
    }
     SubShader
    {
        Tags { "RenderType"="Transparent" }
        LOD 100

        ZWrite Off
        Blend SrcAlpha OneMinusSrcAlpha

        Pass
        {
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag

            #include "UnityCG.cginc"

            half4 _Color;


            struct appdata
            {
                float4 vertex : POSITION;
            };

            struct v2f
            {
                float4 vertex : SV_POSITION;
                float depth : DEPTH;
            };

            v2f vert (appdata v)
            {
                v2f o;
                o.vertex = UnityObjectToClipPos(v.vertex);
                //Multiplies vertex times model and view matrix, leaving it on 
                //camera coordinates, then, multiplying it by projectionParams
                //which is equal to 1 / farplane, which leaves us with a 
                //normalized value
                o.depth = -mul(UNITY_MATRIX_MV, v.vertex).z * _ProjectionParams.w;

                return o;
            }

            fixed4 frag (v2f i) : SV_Target
            {
                return _Color;
            }
            ENDCG
        }
    }
}
