﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Coin : MonoBehaviour, IInteractable
{
    public float InteractionDistance{
        get{
            return 3.0f;
        }
    }

    [SerializeField]
    GameObject particlesPrefab;
    ParticleSystem feedbackParticles;

    void Start(){
        feedbackParticles = Instantiate(particlesPrefab, this.transform.position, Quaternion.identity).GetComponent<ParticleSystem>();
        feedbackParticles.Stop();
    }



    public bool Consume(){
        Destroy(this.gameObject);
       // LevelManager.Instance.ConsumeCoin();
        return true;
    }

    public void Interaction()
    {
        Consume();
    }

    public void ShowInRangeFeedback()
    {
        if(!feedbackParticles.isPlaying)
            feedbackParticles.Play();
    }

    public void StopInRangeFeedback()
    {
        if(feedbackParticles.isPlaying)
            feedbackParticles.Stop();
    }

    new GameObject gameObject{
        get{
            return this.gameObject;
        }
    }
}
