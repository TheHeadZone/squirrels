﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShaderController : MonoBehaviour
{

    Object[] renderers;
    Material[] materials;

    List<Material> targetMaterials = new List<Material>();

    [Range(0, 1)]
    public float transitionValueAToB;

    [Range(0, 1)]
    public float transitionValueBToC;

    [Range(0, 50)]
    public float effectRadius;

    

    public float lineWidth = 0.01f;
    
    [ColorUsageAttribute(true,true)]
    public Color dissolveTint;

    void Awake()
    {
        //targetMaterials = new List<Material>();
        renderers = GameObject.FindObjectsOfType(typeof(Renderer));

        for (int i = 0; i < renderers.Length; ++i)
        {

            materials = ((Renderer)renderers[i]).materials;

            for (int j = 0; j < materials.Length; ++j)
            {
                if (materials[j].shader.name == "Shader Graphs/MultiTextured")
                {
                    materials[j].SetFloat("Vector1_C98321D6", 0);
                    materials[j].SetFloat("Vector1_EF13F7E4", 300);
                    materials[j].SetFloat("Vector1_B89A3ABD", lineWidth);
                    materials[j].SetColor("Color_B72E2382", dissolveTint);
                    targetMaterials.Add(materials[j]);
                }

            }
        }
    }

    void OnValidate()
    {

        for(int i = 0; i < targetMaterials.Count; ++i){
            targetMaterials[i].SetFloat("Vector1_C98321D6", transitionValueBToC);
            targetMaterials[i].SetFloat("Vector1_CC8B41FA", effectRadius);
            targetMaterials[i].SetFloat("Vector1_AD73D53A", transitionValueAToB);
        }
    }
}
