﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : MonoBehaviour
{


    bool hasCoinAvailable = false;

    bool isInRange(IInteractable interactable){
        float distance = Vector3.Distance(this.transform.position, interactable.gameObject.transform.position);
        if(distance < interactable.InteractionDistance)
            return true;
        else
            return false;
    }

    bool isBeingViewed(IInteractable interactable){
        if(Vector3.Dot(this.transform.forward, (interactable.gameObject.transform.position - this.transform.position).normalized) > .5){
            return true;
        }
        else{
            return false;
        }
    }



    void Update()
    {
        if(isInRange(LevelManager.Instance.ActiveCoin)){
            print(LevelManager.Instance.ActiveCoin + "is in range");
            if(isBeingViewed(LevelManager.Instance.ActiveCoin)){
                print(LevelManager.Instance.ActiveCoin + " is being viewed");
                LevelManager.Instance.ActiveCoin.ShowInRangeFeedback();
            }
        }else{
            LevelManager.Instance.ActiveCoin.StopInRangeFeedback();
        }
    }


}
