﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IInteractable
{
    float InteractionDistance{
        get;
    }

    GameObject gameObject{
        get;
    }

    void ShowInRangeFeedback(); //Display visual feedback when in range for interaction

    void StopInRangeFeedback();

    void Interaction(); //do the correspondent action of the object when being interacted with


    
}
