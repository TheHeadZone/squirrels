﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

[ExecuteInEditMode]
public class ReplacementShaderEffect : MonoBehaviour
{
    public Shader ReplacementShader;
    public Color OverDrawColor;
    
    public Transform EffectOrigin;
    [Range(0, 1)]
    public float Weight;
    public float Radius;
    public Vector3 origin;

    void OnValidate()
    {
        Shader.SetGlobalColor("_OverDrawColor", OverDrawColor);
        Shader.SetGlobalVector("_OriginWorldSpace", EffectOrigin.position);
        Shader.SetGlobalFloat("_Weight", Weight);
        Shader.SetGlobalFloat("_Radius", Radius);
    }

    void OnEnable()
    {
        if (ReplacementShader != null)
            GetComponent<Camera>().SetReplacementShader(ReplacementShader, "");
    }

    void OnDisable()
    {
        GetComponent<Camera>().ResetReplacementShader();
    }
}