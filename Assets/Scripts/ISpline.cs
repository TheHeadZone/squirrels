﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface ISpline
{
    Vector3 GetNonUniformPoint(float t);
    Vector3 GetPoint(float t);

    Vector3 GetLeft(float t);
    Vector3 GetRight(float t);
    Vector3 GetUp(float t);
    Vector3 GetDown(float t);
    Vector3 GetForward(float t);
    Vector3 GetBackward(float t);


    Vector3 GetLength(float t);

    Vector3 GetControlPoint(int index);
    Vector3 SetContrlPoint(int index, Vector3 position);
    Vector3 InsertControlPoint(int index, Vector3 position);
    Vector3 RemoveControlPoint(int index);

    Vector3 GetDistance(float distance);
    Vector3 FindClosest(Vector3 worldPoint);

    int ControlPointCount{get;}
}
