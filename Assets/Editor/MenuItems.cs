﻿using UnityEditor;
using UnityEngine;

public class MenuItems
{
    [MenuItem("Tools/Clear PlayerPrefs")]
    private static void NewMenuOption()
    {
        PlayerPrefs.DeleteAll();
    }

    [MenuItem("Tools/Subsystem/Item 01 %p")]
    private static void Routine01()
    {
        Debug.Log("Routine 01");
    }

    [MenuItem("Tools/Subsystem/Item 02 #p")]
    private static void Routine02()
    {
        Debug.Log("Routine 02");

    }

    [MenuItem("Tools/Subsystem/Item 03 &p")]
    private static void Routine03()
    {
        Debug.Log("Routine 03");

    }

    [MenuItem("Tools/Subsystem/Item 04 %UP")]
    private static void Routine04()
    {
        Debug.Log("Routine 04");

    }

    [MenuItem("Assets/Create Config")]
    private static void CreateConfig(){
        Debug.Log("Creating Configuration");
    }

    [MenuItem("Assets/Create/Configuration Panel")]
    private static void ConfigurationPanel(){
        Debug.Log("Creating configuration panel");
    }

    [MenuItem("CONTEXT/Transform/Get matrix")]
    private static void GetMatrix(){
        Debug.Log("Getting matrix...");
    }

    [MenuItem("Assets/CopyTexelData")]
    private static void ProcessTexelData(){

    }

    [MenuItem("Assets/CopyTexelData", true)]
    private static bool NewMenuOptionValidation(){
        return Selection.activeObject.GetType() == typeof(Texture2D);
    }

    [MenuItem("CONTEXT/Transform/New Position")]
    private static void NewPosition(MenuCommand menuCommand){
        var transform = menuCommand.context as Transform;
        transform.position = Vector3.zero; 
    }
}
